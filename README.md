# Miniframe Twig Bundle

This library adds Twig support to the [Miniframe PHP Framework](https://miniframe.dev/).

[![build](https://miniframe.dev/build/badge/twig-bundle)](https://bitbucket.org/miniframe/twig-bundle/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/twig-bundle)](https://miniframe.dev/codecoverage/twig-bundle)

## How to use

1. In your existing project, type: `composer require miniframe/twig-bundle`
2. Add the directive below to your config.ini
3. Put templates in `$projectroot/templates`

*Example config.ini directives*
```ini
[framework]
base_href        = /
middleware[twig] = Miniframe\Middleware\Twig

[twig]
cache_path      = cache/twig/
template_path[] = templates/
```

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/twig-bundle/addon/pipelines/home) to automatically test this project.
