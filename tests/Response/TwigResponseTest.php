<?php

namespace Miniframe\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Registry;
use Miniframe\Middleware\Twig;
use Miniframe\Middleware\TwigTest;
use PHPUnit\Framework\TestCase;

class TwigResponseTest extends TestCase
{
    /**
     * Clean up registry after each test
     *
     * @return void
     */
    protected function tearDown(): void
    {
        if (Registry::has('twig')) {
            Registry::register('twig', null);
        }
        if (Registry::has(Twig::class)) {
            Registry::register(Twig::class, null);
        }
    }

    /**
     * When Twig is not loaded as middleware, creating a Twig response causes an exception
     *
     * @return void
     */
    public function testNoRegister(): void
    {
        $this->expectException(\RuntimeException::class);
        new TwigResponse('index.html.twig');
    }

    /**
     * Test the render() method when the extension is registered as 'twig'
     *
     * @return void
     */
    public function testRenderRegisterAsString(): void
    {
        // Initialize Twig
        Registry::register('twig', new Twig(new Request(['REQUEST_URI' => '/']), TwigTest::getDummyConfig()));

        $response = new TwigResponse('index.html.twig', ['foo' => 'bar']);
        $html = $response->render();

        $this->assertEquals('<strong>bar</strong>', $html);
    }

    /**
     * Test the render() method when the extension is registered as 'Twig::class'
     *
     * @return void
     */
    public function testRenderRegisterAsClass(): void
    {
        // Initialize Twig
        Registry::register(Twig::class, new Twig(new Request(['REQUEST_URI' => '/']), TwigTest::getDummyConfig()));

        $response = new TwigResponse('index.html.twig', ['foo' => 'bar']);
        $html = $response->render();

        $this->assertEquals('<strong>bar</strong>', $html);
    }
}
