<?php

namespace Miniframe\Middleware;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

class TwigTest extends TestCase
{
    /**
     * Tests if we can correctly create a twig environment
     *
     * @return void
     */
    public function testGetEnvironment(): void
    {
        $middleware = new Twig(new Request(['REQUEST_URI' => '/']), static::getDummyConfig());

        $this->assertInstanceOf(Environment::class, $middleware->getEnvironment());
    }

    /**
     * Returns a fictive configuration based on an array (can be used for several tests)
     *
     * @return Config
     */
    public static function getDummyConfig(): Config
    {
        return Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__ . '/../',
            'data' => [
                'framework' => ['base_href' => '/'],
                'twig' => ['template_path' => 'templates' ] // Relative from projectFolder
            ],
        ]);
    }
}
