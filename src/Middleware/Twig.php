<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Twig extends AbstractMiddleware
{
    /**
     * Reference to the Twig Environment
     *
     * @var Environment
     */
    private $twigEnvironment;

    /**
     * Initializes the Twig Bundle
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Fetch required config values
        $baseHref = $config->get('framework', 'base_href');
        $templatePaths = $config->getPath('twig', 'template_path');
        $cachePath = $config->has('twig', 'cache_path') ? $config->getPath('twig', 'cache_path') : false;

        // Initialize Twig
        $twig = new Environment(new FilesystemLoader($templatePaths), [
            'cache' => $cachePath,
        ]);
        $twig->addGlobal('baseHref', $baseHref);
        $this->twigEnvironment = $twig;
    }

    /**
     * Gets the Twig Environment
     *
     * @return Environment
     */
    public function getEnvironment(): Environment
    {
        return $this->twigEnvironment;
    }
}
