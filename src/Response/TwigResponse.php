<?php

namespace Miniframe\Response;

use Miniframe\Core\Registry;
use Miniframe\Core\Response;
use Miniframe\Middleware\Twig;

class TwigResponse extends Response
{
    /**
     * Filename of the template
     *
     * @var string
     */
    private $template;

    /**
     * Properties sent to the template engine
     *
     * @var array
     */
    private $properties;

    /**
     * Reference to the Twig middleware
     *
     * @var Twig|null
     */
    private $twigMiddleware = null;

    /**
     * Initializes a new Twig response
     *
     * @param string $template   Filename of the template.
     * @param array  $properties Properties to be sent to the template.
     */
    public function __construct(string $template, array $properties = array())
    {
        if (Registry::has('twig')) {
            $this->twigMiddleware = Registry::get('twig');
        } elseif (Registry::has(Twig::class)) {
            $this->twigMiddleware = Registry::get(Twig::class);
        }
        if ($this->twigMiddleware === null) {
            throw new \RuntimeException(
                'Twig middleware not loaded. See '
                . 'https://bitbucket.org/miniframe/twig-bundle/src/master/README.md'
                . ' for the config.ini directives.'
            );
        }

        $this->template = $template;
        $this->properties = $properties;
    }

    /**
     * Returns the rendered version of the template.
     *
     * @return string
     */
    public function render(): string
    {
        return $this->twigMiddleware->getEnvironment()->render($this->template, $this->properties);
    }
}
